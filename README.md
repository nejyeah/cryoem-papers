﻿# Paper List for cryo-EM

标签（空格分隔）： paper

---
[TOC]

---
## 1. method
- High Resolution Single Particle Cryo-Electron Microscopy using BeamImage
Shift, bioRxiv2018April, [paper](https://www.biorxiv.org/content/early/2018/04/23/306241), [code](), [blog]()
- Automated tracing of helical assemblies from electron cryo-micrographs, JSB2018April, [paper](https://www.sciencedirect.com/science/article/pii/S1047847717302095), [code](), [blog]()
- APPLE Picker: Automatic Particle Picking, a Low-Effort Cryo-EM Framework, arXiv2018Feb, [paper](https://arxiv.org/abs/1802.00469), [code](), [blog]()
- Positive-unlabeled convolutional neural networks for particle picking in cryo-electron micrographs, arXiv2018Mar, [paper](https://arxiv.org/abs/1803.08207), [code](), [blog]()
- DEEPre: sequence-based enzyme EC number prediction by deep learning, Bioinformatics 2017Oct, [paper](https://www.ncbi.nlm.nih.gov/pubmed/29069344), [code](), [blog]()
- CryoEM-based hybrid modeling approaches for structure determination,  Current Opinion in Microbiology 2017Oct, [paper](https://www.sciencedirect.com/science/article/pii/S1369527417300607), [code](), [blog]()
- Big data in cryoEM: automated collection, processing and accessibility of EM data, Current Opinion in Microbiology 2017Oct, [paper](https://www.sciencedirect.com/science/article/pii/S1369527417301315), [code](), [blog]()
- A fast fiducial marker tracking model for fully automatic alignment in electron tomography, Bioinformatics 2017Oct, [paper](https://academic.oup.com/bioinformatics/article/34/5/853/4562325), [code](), [blog]()
- Near-Atomic Resolution Structure Determination in Over-Focus with Volta Phase Plate by Cs-Corrected Cryo-EM，Structure 2017Oct, [paper](https://www.nature.com/articles/nmeth.4405), [code](), [blog]()
- Convolutional neural networks for automated annotation of cellular cryo-electron tomograms, nature methods 2017Aug, [paper](https://www.nature.com/articles/nmeth.4405), [code](), [blog]()

- EMHP: An accurate automated hole masking algorithm for single-particle cryo-EM image processing, bioRxiv2017Jul, [paper](https://www.biorxiv.org/content/early/2017/07/28/154211), [code](), [blog]()
- Helical reconstruction in RELION, JSB2017June, [paper](https://www.sciencedirect.com/science/article/pii/S1047847717300199), [code](), [blog]()
- A fast method for particle picking in cryo-electron micrographs based on fast R-CNN, AIP Conference Proceedings 2017Jun, [paper](https://aip.scitation.org/doi/abs/10.1063/1.4982020), [code](), [blog]()
- Biological cryo‐electron microscopy in China, Protein Science 2016August, [paper](https://onlinelibrary.wiley.com/doi/full/10.1002/pro.3018), [code](), [blog]()
- Birth and Future of Multiscale Modeling forMacromolecular Systems, Nobel Lecture 2014, [paper](https://onlinelibrary.wiley.com/doi/epdf/10.1002/anie.201403691), [code](), [blog]()







